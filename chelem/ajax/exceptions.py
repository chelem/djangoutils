__author__ = 'zmbq'


class HttpStatusException(Exception):
    pass


class HttpBadRequestException(HttpStatusException):
    code = 400
