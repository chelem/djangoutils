from django.http import JsonResponse

__author__ = 'zmbq'


class JsonStatusResponse(JsonResponse):
    """ A JsonResponse with a non-200 status code.
    status_code should be defined in the derived classes
    """
    def __init__(self, *args, **kwargs):
        if 'status' not in kwargs:
            kwargs['status'] = self.json_status_code
        if not args:
            args = [{}]  # Add data if it's not there
        super().__init__(*args, **kwargs)


class JsonNoContentResponse(JsonStatusResponse):
    json_status_code = 204

    def __init__(self, *args, **kwargs):
        if args:
            raise ValueError("JsonNoContentResponse must not receive a response")
        super().__init__(*args, **kwargs)


class JsonResponseNotAuthorized(JsonStatusResponse):
    json_status_code = 401


class JsonResponseBadRequest(JsonStatusResponse):
    json_status_code = 400


class JsonResponseNotFound(JsonStatusResponse):
    json_status_code = 404


class JsonResponseForbidden(JsonStatusResponse):
    json_status_code = 403


class JsonResponseNotAcceptable(JsonStatusResponse):
    json_status_code = 406


class JsonResponseConflict(JsonStatusResponse):
    json_status_code = 409