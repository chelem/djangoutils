""" A middleware that returns HTTP codes when an exception is thrown.
    Influenced by http://stackoverflow.com/a/4209389/871910
"""
from django.http import HttpResponse
from general.http.exceptions import HttpStatusException

__author__ = 'zmbq'


class ExceptionToStatusMiddleware:
    def process_exception(self, request, exception):
        if isinstance(exception, HttpStatusException):
            return HttpResponse(status=exception.code)
