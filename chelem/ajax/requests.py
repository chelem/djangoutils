from django.views.decorators.http import require_http_methods

__author__ = 'zmbq'

require_DELETE = require_http_methods(['DELETE'])
require_DELETE.__doc__ = "Decorator to require that a view only accept the DELETE method."

require_PUT = require_http_methods(['PUT'])
require_PUT.__doc__ = "Decorator to require that a view only accept the PUT method."
