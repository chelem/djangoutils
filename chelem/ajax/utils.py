import json

__author__ = 'zmbq'

def request_to_json(request):
    """
    Extracts the json body from a request
    :param request: Request
    :return: Parsed JSON
    """
    s = request.body.decode('utf-8')  # http://stackoverflow.com/a/29514222/871910
    return json.loads(s)
