# Useful utilities for testing a REST web application
import json

methods = {
    'POST': lambda c: c.post,
    'GET': lambda c: c.get,
    'DELETE': lambda c: c.delete,
    'PUT': lambda c: c.put,
}


def send_json_and_files(client, method, url, data, path):
    """
        Send JSON as well as files to the server
        :param client: Client instance
        :param method: HTTP verb
        :param url: URL
        :param data: Body data (optional)
        :param files: Files to upload
        :return: The HTTP Response
        """
    func = methods[method](client)
    data.update({'file': open(path, 'rb')})
    return func(url, data=data)


def send_json_files(client, method, url, files):
    """
    Send files to the server
    :param client: Client instance
    :param method: HTTP verb
    :param url: URL
    :param files: Files to upload
    :return: The HTTP Response
    """
    func = methods[method](client)
    return func(url, data=files)


def send_json(client, method, url, data=None):
    """
    Send JSON to the server
    :param client: Client instance
    :param method: HTTP verb
    :param url: URL
    :param data: Body data (optional)
    :return: HTTP Response
    """
    func = methods[method](client)
    if data:
        jdata = json.dumps(data)
    else:
        jdata = None
    return func(url, data=jdata, content_type='application/json')


def get_response_data(response):
    """
    Get the response's JSON body
    :param response: HTTP Response
    :return: The JSON body loaded into a dictionary
    """
    content = response.content.decode()
    if content:
        return json.loads(response.content.decode('utf-8'))
    return {}
