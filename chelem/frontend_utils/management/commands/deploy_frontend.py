from django.conf import settings
from django.core import management
from django.core.management import BaseCommand
import os

class Command(BaseCommand):
    help = 'Deploy the Frontend files'

    def bundle_js(self):
        self.stdout.write("Running jspm bundle to bundle all scripts")
        os.chdir(os.path.join(settings.FRONTEND_DIR, 'static'))
        os.system("jspm bundle app/app.module app.bundle.js --minify")

    def collect_static(self):
        management.call_command('collectstatic', '--no-input')

    def handle(self, *args, **options):
        cwd = os.getcwd()
        try:
            self.bundle_js()
            self.collect_static()
        finally:
            os.chdir(cwd)
