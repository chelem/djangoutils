
def homepage(request):
    from django.shortcuts import render_to_response
    from django.conf import settings

    try:
        bundle = settings.BUNDLE
    except AttributeError:
        bundle = not settings.DEBUG

    return render_to_response('index.html', {'BUNDLE': bundle})