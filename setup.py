import os
from setuptools import setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='chelem-django-utils',
    version='0.4.4',
    packages=['chelem.ajax', 'chelem.tests','chelem.frontend_utils'],
    include_package_data=True,
    install_requires=['django>=1.8'],
    license='Chelem',  # example license
    description='Several Django utilities for the Chelem projects',
    long_description=README,
    url='https://bitbucket.org/chelem/djangoutils',
    author='Itay Zandbank',
    author_email='itay@chelem.co.il',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: Other/Proprietary License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.4',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)
