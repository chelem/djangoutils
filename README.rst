=================
Django Ajax Utils
=================

A few utility classes and functions that have proved to be useful in our Djagno developments.

Currently there's only one package here:

http package
------------

This package contains code that helps with the HTTP protocol.

In http.utils there's the request_to_json function that converts a request's body (whether it's POST, PUT, etc...)
to a dictionary.

http.responses contains several JsonResponse derived classes that easy up returning on HTTP status codes.

http.middleware converts some exceptions (those in http.exceptions) into HttpResponses.

http.requests contains decorators that we feel are missing from Django - @require_PUT and @require_DELETE.


tests package
-------------
This package contains a few handy methods in the test.utils file - for sending and parsing JSONs in unit tests.
It also contains a mixin for the TestRunner that uses a temporary media folder, so that media files generated during the test are deleted.
To use add the following to your settings.py:

    class TestDiscoverRunner(TempMediaDiscoverRunnerMixin, DiscoverRunner):
        pass
    TEST_RUNNER = 'project.settings.TestDiscoverRunner'


Installation (for the Mixin)
------------

1. Add "chelem-django-ajax-utils" to your settings.py:

    INSTALLED_APPS = (
        ...
        'chelem-django-utils',
    )


License
-------
This code can be freely incorporated into any Chelem project.
